cryptokit (1.20.1-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Sat, 13 Jul 2024 08:03:00 +0200

cryptokit (1.19-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Thu, 15 Feb 2024 03:52:06 +0100

cryptokit (1.18-3) unstable; urgency=medium

  * Use ocaml_dune DH buildsystem
  * Bump Standards-Version to 4.6.2

 -- Stéphane Glondu <glondu@debian.org>  Mon, 07 Aug 2023 08:18:01 +0200

cryptokit (1.18-2) unstable; urgency=medium

  * Fix build with recent dune (Closes: #1041001)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 14 Jul 2023 09:36:55 +0200

cryptokit (1.18-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Thu, 02 Feb 2023 02:53:03 +0100

cryptokit (1.16.1-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper compat level to 13

 -- Stéphane Glondu <glondu@debian.org>  Sat, 07 Nov 2020 13:31:18 +0100

cryptokit (1.15-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.5.0

 -- Stéphane Glondu <glondu@debian.org>  Mon, 10 Feb 2020 05:16:26 +0100

cryptokit (1.14-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.4.1
  * Add Rules-Requires-Root: no
  * Build-depend on debhelper-compat and remove debian/compat

 -- Stéphane Glondu <glondu@debian.org>  Thu, 19 Dec 2019 11:34:33 +0100

cryptokit (1.13-2) unstable; urgency=medium

  * Fix FTBFS on bytecode architectures

 -- Stéphane Glondu <glondu@debian.org>  Mon, 05 Aug 2019 17:24:05 +0200

cryptokit (1.13-1) unstable; urgency=medium

  * New upstream release
  * Update Vcs-*
  * Remove Samuel from Uploaders
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.4.0

 -- Stéphane Glondu <glondu@debian.org>  Sun, 04 Aug 2019 11:12:07 +0200

cryptokit (1.11-1) unstable; urgency=medium

  * New upstream release
  * Change upstream website to github
  * Add ocamlbuild and zarith to Build-Depends
  * Update Vcs-*
  * Bump debhelper compat to 10
  * Bump Standards-Version to 4.0.0

 -- Stéphane Glondu <glondu@debian.org>  Fri, 14 Jul 2017 14:38:54 +0200

cryptokit (1.10-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 3.9.6 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Sat, 02 May 2015 16:10:00 +0200

cryptokit (1.9-2) unstable; urgency=low

  * Upload to unstable

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Dec 2013 08:11:18 +0100

cryptokit (1.9-1) experimental; urgency=low

  * New upstream release
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Sat, 09 Nov 2013 10:52:39 +0100

cryptokit (1.7-2) experimental; urgency=low

  * Compile with OCaml >= 4

 -- Stéphane Glondu <glondu@debian.org>  Thu, 25 Jul 2013 21:32:18 +0200

cryptokit (1.7-1) unstable; urgency=low

  [ Stéphane Glondu ]
  * New upstream release
  * Use format version 1.0 in debian/copyright
  * Bump Standards-Version to 3.9.4

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

 -- Stéphane Glondu <glondu@debian.org>  Wed, 08 May 2013 14:48:43 +0200

cryptokit (1.5-1) unstable; urgency=low

  [ Stéphane Glondu ]
  * New upstream release
    - relocated to forge.o.o, update Homepage and debian/watch accordingly
  * Bump debhelper compat level to 8
  * Switch source package format to 3.0 (quilt)
  * Bump Standards-Version to 3.9.2

  [ Stefano Zacchiroli ]
  * remove myself from Uploaders

 -- Stéphane Glondu <glondu@debian.org>  Mon, 21 Nov 2011 21:23:10 +0100

cryptokit (1.3-14) unstable; urgency=low

  [ Stéphane Glondu ]
  * Add missing dependency on zlib1g-dev (Closes: #536505)
  * Remove control.in
  * debian/control:
    - remove Remi Vanicat and Sven Luther from Uploaders
    - move to section ocaml
    - update Standards-Version to 3.8.3
    - update for dh-ocaml 0.9, and add versioned build-dependency

  [ Mehdi Dogguy ]
  * Update email addresses and remove DMUA

 -- Stéphane Glondu <glondu@debian.org>  Fri, 09 Oct 2009 11:17:23 +0200

cryptokit (1.3-13) unstable; urgency=medium

  * Install all *.a files (including the one attached to cryptokit.cmxa)
    (Closes: #527816)

 -- Stephane Glondu <steph@glondu.net>  Tue, 12 May 2009 11:27:02 +0200

cryptokit (1.3-12) unstable; urgency=low

  * Used ifeq instead of ifdef in debian/rules: a variable can be defined
    and empty. Fixes FTBFS on non-native archs, Closes: #524247.
  * Add myself to Uploaders.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Wed, 15 Apr 2009 22:31:51 +0200

cryptokit (1.3-11) unstable; urgency=low

  * Add myself to Uploaders and DMUA
  * Move to new archive section ocaml
  * Move .cma and META to libcryptokit-ocaml, build .cmxs
  * Remove unused patches and dpatch dependency
  * Remove clean target from debian/rules (not needed)
  * Bump Standards-Version to 3.8.1

 -- Stephane Glondu <steph@glondu.net>  Sun, 12 Apr 2009 00:36:22 +0200

cryptokit (1.3-10) unstable; urgency=low

  * Add missing build-dependency on dh-ocaml.

 -- Samuel Mimram <smimram@debian.org>  Wed, 25 Feb 2009 18:22:56 +0100

cryptokit (1.3-9) unstable; urgency=low

  [ Stephane Glondu ]
  * Remove Julien from Uploaders

  [ Samuel Mimram ]
  * Rebuild with OCaml 3.11.
  * Switch packaging to git
  * Update compat to 7.
  * Update standards version to 3.8.0.

 -- Samuel Mimram <smimram@debian.org>  Tue, 24 Feb 2009 19:09:23 +0100

cryptokit (1.3-8) unstable; urgency=medium

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Stephane Glondu ]
  * Enable zlib support, closes: #469040.

  [ Sylvain Le Gall ]
  * Add homepage field in debian/control

 -- Sylvain Le Gall <gildor@debian.org>  Mon, 03 Mar 2008 11:49:24 +0100

cryptokit (1.3-7) unstable; urgency=low

  * Depend on ocaml-nox instead of ocaml, closes: #450594.
  * Updated stantards version to 3.7.3, no changes needed.

 -- Samuel Mimram <smimram@debian.org>  Sat, 22 Dec 2007 00:49:16 +0100

cryptokit (1.3-6) unstable; urgency=low

  * Build for ocaml 3.10.0
  * Generate documentation with ocamldoc, doesn't ship the one provided by
    upstream (generate almost the same thing, but with a more recent version
    of ocamldoc and in the right place)

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 04 Sep 2007 00:26:44 +0200

cryptokit (1.3-5) experimental; urgency=low

  * Add dependency on ocaml package

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 24 Jul 2007 00:56:00 +0200

cryptokit (1.3-4) experimental; urgency=low

  [ Sylvain Le Gall ]
  * Upgrade debian/watch version to 3,
  * Upgrade debhelper debian/compat to 5,
  * Replace dependency Source-Version by source:Version,
  * Use CDBS for debian/rules,
  * Use @OCamlTeam@ for Uploaders field in debian/control.in,
  * Add versioned Build-Depends on dpkg-dev (>= 1.13.19),
  * Rebuild for ocaml 3.10.0

  [ Samuel Mimram ]
  * Updated doc-base entry, closes: #318423.

  [ Julien Cristau ]
  * Remove inactive people from the Uploaders field.

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 07 Jul 2007 23:26:24 +0200

cryptokit (1.3-3) unstable; urgency=low

  * Rebuild with OCaml 3.09.2.
  * Updated standards version to 3.7.2, no changes needed.
  * We don't need to remove rpaths anymore.

 -- Samuel Mimram <smimram@debian.org>  Fri, 19 May 2006 14:09:09 +0000

cryptokit (1.3-2) unstable; urgency=low

  * Rebuild for OCaml 3.09.1

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Tue, 10 Jan 2006 23:55:51 +0100

cryptokit (1.3-1) unstable; urgency=low

  * New upstream release
  * Upgrade standards version to 3.6.2.0 (no change)
  * Stop using numerix, remove dependencies and patches related to numerix
  * Use svn-buildpackage
  * Get rid of hardcoded OCaml ABI
  * Rewrite the META to META.in and place it in METAS/, add "num" to the list
    of requires (and remove "numerix").

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Tue, 20 Dec 2005 00:45:55 +0100

cryptokit (1.2-8) unstable; urgency=medium

  * Updated to OCaml 3.08.3.
  * Using dh_shlibdeps.
  * Added -g and -fPIC to the CFLAGS.
  * Installing META in /usr/lib/ocaml/XXX/cryptokit.

 -- Samuel Mimram <smimram@debian.org>  Tue,  5 Apr 2005 22:02:17 +0200

cryptokit (1.2-7) unstable; urgency=high

  * Rebuilding with ocaml 3.08.2 to get rid of the inconsistent assumptions over
    the Unix module, closes: #285780.

 -- Samuel Mimram <smimram@debian.org>  Sat,  1 Jan 2005 18:14:01 +0100

cryptokit (1.2-6) unstable; urgency=medium

  * Corrected a bug in the patch to handle too big buffers.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Tue, 16 Nov 2004 20:25:15 +0100

cryptokit (1.2-5) unstable; urgency=medium

  * Added a patch to raise an exception when the input buffer is too big for
    the put_string method of uncompress, see:
    http://caml.inria.fr/archives/200411/msg00008.html

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Wed,  3 Nov 2004 14:25:49 +0100

cryptokit (1.2-4) unstable; urgency=low

  * Made the dependency on numerix stronger to ensure we get the 3.08 version

 -- Mike Furr <mfurr@debian.org>  Tue, 27 Jul 2004 19:49:33 -0400

cryptokit (1.2-3) unstable; urgency=low

  * Rebuild against ocaml 3.08 (Closes: #261208)
  * Changed deps to ocaml-nox
  * Moved meta file to correct location (Closes: #247133)

 -- Mike Furr <mfurr@debian.org>  Tue, 27 Jul 2004 11:56:44 -0400

cryptokit (1.2-2) unstable; urgency=low

  * Changed debian/rules to only call allopt target on platforms that have the
    ocamlopt compiler (Closes: 243542)
  * Added myself to Uploaders list.

 -- Mike Furr <mfurr@debian.org>  Wed, 14 Apr 2004 19:01:30 -0400

cryptokit (1.2-1) unstable; urgency=low

  * First upload (closes: Bug#203256)
  * debian/control: explicit Section: lines for binary packages
  * debian/control: Maintainer: Debian OCaml Maintainers, etc.
  * Sign with DSA subkey of new GPG key

 -- Michael K. Edwards <medwards-debian@sane.net>  Mon, 19 Jan 2004 11:48:52 -0800

cryptokit (1.2-0.4) unstable; urgency=low

  * Build against Numerix packages with Big (new nums library) support
  * Include examples and speed test results in docs
  * Build with Numerix.Slong on i386, Numerix.Gmp elsewhere

 -- Michael K. Edwards (in Debian context) <mkedeb@sane.net>  Thu, 18 Dec 2003 02:34:23 -0800

cryptokit (1.2-0.3) unstable; urgency=low

  * Split into libcryptokit-ocaml{,dev}, and build against split Numerix
    packages.

 -- Michael K. Edwards (in Debian context) <mkedeb@sane.net>  Wed, 17 Dec 2003 13:00:32 -0800

cryptokit (1.2-0.2) unstable; urgency=low

  * Ported to Numerix from (non-free) Nat bignum library

 -- Michael K. Edwards (in Debian context) <mkedeb@sane.net>  Mon, 15 Dec 2003 01:40:56 -0800

cryptokit (1.2-0.1) unstable; urgency=low

  * Initial Release

 -- Michael K. Edwards (in Debian context) <mkedeb@sane.net>  Sat, 13 Dec 2003 14:33:27 -0800
